# Beta Icons Restored and Reimagined

Beta Icons Restored and Reimagined provides an alternate glimpse into what might have been. It's based on the Beta skill icons, which you can find [here](https://tcrf.net/Prerelease:The_Elder_Scrolls_III:_Morrowind/Art_and_Textures) for reference. However, the beta icons had problems of their own. For one, the rune backgrounds used obfuscated the art a lot for spell icons. Remember, these are displayed at 32x32px for the big spell icons and 16x16px for the small ones. Thus, I took the color palettes of the beta skill icons and extended them to the spell, attribute, and dynamic stat icons. The result is a full suite of character icon replacements (293 in total), best suited for use with [Monochrome UI](https://www.nexusmods.com/morrowind/mods/45174). See below for a preview of all the new icons!

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/birr)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\birr

        # Linux
        /home/username/games/OpenMWMods/birr

        # macOS
        /Users/username/games/OpenMWMods/birr

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\birr"`)

#### Icon Preview

<div align="center"> <img src="../img/birrPreview.png" alt="Replacement icons added by BIRR." /> </div>

<div id="credits" style="text-align: center;">

#### Credits

Author: **S3ctor**

All assets was made by Dave Corley under the GPL3 license. Please enjoy my mod, hack away as you please, and respect the freedoms of your fellow modders and players in the meantime.

</div>
