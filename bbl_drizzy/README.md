# BBL Drizzy Soundbanks

Inspired by possibly the greatest rap beef of all time, the BBL Drizzy Soundbank set is an exceedingly simple mod. I picked my five favorite BBL Drizzies and made them into a soundbank for Dynamic Music. I *think* these soundbanks may work with MUSE2 as well, let me know if they don't and I'll see what I can do.

## Usage

<!-- Really should have a link to DM here as well -->
BBL Drizzy Soundbanks requires Dynamic Music For OpenMW-Lua, which in turn, requires OpenMW 0.49. These soundbanks are built for Dynamic Music version 11, which is still current as of OpenMW-cd116ebe5f.

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/bbl-drizzy)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\bbl-drizzy

        # Linux
        /home/username/games/OpenMWMods/bbl-drizzy

        # macOS
        /Users/username/games/OpenMWMods/bbl-drizzy

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\bbl-drizzy"`)

<p align="center">
  <img src="../img/modathonbanner2024.png" alt="Modathon 2024" />
</p>

<div id="credits" style="text-align: center;">

#### Credits

Author: **S3ctor**

All code was written by Dave Corley under the GPL3 license. Please enjoy my mod, hack away as you please, and respect the freedoms of your fellow modders and players in the meantime.

The tracks used in BBL Drizzy Soundbanks are hosted here with permission of their original authors and are licensed under their original terms.

Please show some love to these creators, and Metro Boomin for creating the original track.

<!-- Fuck Drake -->

</div>
