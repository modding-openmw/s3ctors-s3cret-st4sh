local soundBank = {
  id = "s3_drizzy",
  cellNamePatterns = {
    'Dagoth'
  },
  cellNamePatternsExclude = {},
  tracks = {
    {
      path="Music/B.E.A.R - BBL Drizzy Freestyle.mp3",
      length=156
    },
    {
      path="Music/KA'REEF x METRO BOOMIN - BBL DRIZZY FREESTYLE.mp3",
      length=206
    },
    {
      path="Music/Only_Wallace - 'BBL DRIZZY'.mp3",
      length=202
    },
    {
      path="Music/Scru Face Jean - BBL Drizzy Freestyle.mp3",
      length=171
    },
    {
      path="Music/YUMI X PACKGOD BBL DRIZZY (METRO BOOMIN).mp3",
      length=97
    },
  }
}

return soundBank
