# Luamics

Luamics is a simple global script that adds a chance for any chest encountered in-game to be replaced by a mimic chest. Mimic chests will retain their original loot, and spawn at a rate of 5%. Chests which are scripted, owned, or have `trader` in their record id are ineligible to be converted to mimics, so the script *should* be relatively safe. Should be, that is.

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/luamics)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\luamics

        # Linux
        /home/username/games/OpenMWMods/luamics

        # macOS
        /Users/username/games/OpenMWMods/luamics

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\luamics"`)
1. Add `content=luamics.omwaddon` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Credits

Author: **S3ctor**

All assets was made by Dave Corley under the GPL3 license. Please enjoy my mod, hack away as you please, and respect the freedoms of your fellow modders and players in the meantime.

Mimic assets used with permission from [Concit's original mimic mod](https://www.nexusmods.com/morrowind/mods/53889?tab=description)

</div>
