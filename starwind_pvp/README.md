# Starwind PVP

Starwind PVP is inspired by Testman's tes3mp deathmatch script. Presently contained in this archive are only the assets developed for it.

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/starwind_pvp)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\starwind_pvp

        # Linux
        /home/username/games/OpenMWMods/starwind_pvp

        # macOS
        /Users/username/games/OpenMWMods/starwind_pvp

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\starwind_pvp"`)

<div id="credits" style="text-align: center;">

#### Credits

Author: **TSI Team**

- Meshes provided by Ignatious
- Current codebase worked by skoomabreath
- Thanks to Testman for the [original script](https://github.com/testman42/tes3mp-deathmatch)

</div>
