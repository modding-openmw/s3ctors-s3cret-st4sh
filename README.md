# S3ctor's S3cret St4sh

Collection of all mods OpenMW Mods developed by S3ctor, Lua and otherwise. From now to the end of time, you'll find everything I make for Morrowind here. That's a promise, Nexus. You can bookmark [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/) to keep up-to-date with all my solo and collaborative releases.

#### Available Mods

If you just wanna download everything, you can do so from the above link. Follow these links for a description and individual download for each of my mods.

<div id="modMarker"></div>

#### Credits

Author: **S3 Nation**

I made this repository and website, but all my works are built upon the shoulders of giants. 
I'm just a very persistent monkey with a compiler.

Special thanks to:
- [JohnnyHostile](https://gitlab.com/modding-openmw), owner of [modding-openmw.com](https://modding-openmw.com) for making the template this repo is based on, and welcoming me so graciously as part of the MOMW team. 
- [Settyness](https://anfinitinetwork.com/forum/), my newest partner in crime and an excellent shitposter. He's a true inspiration and a really funny dude.
- The rest of the [MOMW](https://modding-openmw.com/about/) and [OpenMW](https://openmw.org/the-team/) team, all of whom I consider to be excellent colleagues and friends.
- [Epoch](https://github.com/EpochWon), who's inspired my own (minor) interest in post-processing, contributed to some components of this repo, and directly inspired the creation of [Morrobroom](https://github.com/magicaldave/Morrobroom/releases/tag/Latest), my [Trenchbroom](https://trenchbroom.github.io) compiler for Morrowind.
- [Ignatious](https://next.nexusmods.com/profile/IgnatiousS/), the creator of Starwind, my mentor, and very close friend. 
- The rest of the [Morrowind community](https://discord.gg/pqkUvKfG3q), for whom this is a gift. I genuinely hope you all enjoy what I've put together for you here.
- The [MWSE team](https://mwse.github.io/MWSE/#authors), who are all really cool folks that carried our community for years while OpenMW was waking itself up. You all constantly inspire me to break my own limits, and the engine's.
- AltheaR, whose zealous moderation on Nexus inspired the creation of this repository. None of this would have happened without you.
