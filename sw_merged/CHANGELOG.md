## Starwind Merged Plugin Project Changelog

# Version 0.5.1
- Restore Starwind Vvardenfell plugin
- Re-add the krayt dragon cell as it was never supposed to be deleted

# Version 0.5.0

1. Publish on S3St4sh
2. Too much to count
