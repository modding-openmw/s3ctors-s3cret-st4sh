# CHIM 2090

I just don't wanna write docs for this right now.

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/birr)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\birr

        # Linux
        /home/username/games/OpenMWMods/birr

        # macOS
        /Users/username/games/OpenMWMods/birr

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\birr"`)

#### Icon Preview

<div align="center"> <img src="../img/birrPreview.png" alt="Replacement icons added by BIRR." /> </div>

<div id="credits" style="text-align: center;">

#### Credits

Author: **S3ctor**

All assets was made by Dave Corley under the GPL3 license. Please enjoy my mod, hack away as you please, and respect the freedoms of your fellow modders and players in the meantime.

</div>
