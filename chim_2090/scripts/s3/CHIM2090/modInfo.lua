--[[

Mod: CHIM 2090
Author: S3ctor

--]]

return {
	name = "CHIM-2090",
    l10nName = "CHIM2090",
	version = "0.4.2",
    logPrefix = "[CHIM 2090]:",
}
