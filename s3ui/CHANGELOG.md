## Transmog Changelog

#### Version 0.4

Initial version of the mod published in OpenMW's Discord

#### Version 0.45

Completely rewrite the entire thing with almost no functional difference

#### Version 0.5

Publish via S3St4sh
