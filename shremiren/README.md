# ShreMiren Menu Replacer

<p align="center">
  <img src="../img/shrefrieren.png" alt="Image of Shrek and Frieren riding a vespa" />
</p>

<p align="center">
  <img src="../img/shremiren.png" alt="Image of Shrek and Hatsune Miku riding a vespa" />
</p>

        "All those moments will be lost in time... like tears in the rain."
        - Shrek to Hatsune Miku

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/s3ctors-s3cret-st4sh/shremiren)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\shremiren

        # Linux
        /home/username/games/OpenMWMods/shremiren

        # macOS
        /Users/username/games/OpenMWMods/shremiren
1. Decide whether to use the 1080p or 4K versions of the replacer
1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\shremiren\1080p"`)

#### Credits

Author: **S3ctor**

All code was written by Dave Corley under the GPL3 license. Please enjoy my mod, hack away as you please, and respect the freedoms of your fellow modders and players in the meantime.
